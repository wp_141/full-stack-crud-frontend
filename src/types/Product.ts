export default interface Product {
  id?: number;
  name: string;
  price: number;
  createdData?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
